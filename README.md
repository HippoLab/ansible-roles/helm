Helm
====

An Ansible role to install Helm - popular package manager for Kubernetes. It is likely that the role will work on any
linux based operating systems as Helm is statically linked binary, however at the moment it was tested only on:

- Debian 9
- CentOS 7

## Contents
- [Role variables](#variables)
- [Dependencies](#dependencies)
- [Playbook Example](#example)
- [License](#license)
- [Author Information](#author)

## <a name="variables"></a> Role Variables

| Variable     | Default value | Description                          |
| ------------ | ------------- | ------------------------------------ |
| helm_version | "3.1.2"       | Version to install                   |
| helm_bin_dir | "/opt/helm"   | Directory Helm will be downloaded to |

## <a name="dependencies"></a> Dependencies
This role does not have any specific dependencies

## <a name="example"></a> Playbook Example
```yaml
- hosts: localhost
  connection: local
  become: True
  roles:
    - role: helm
      tags:
        - helm
```

## <a name="license"></a> License
The role is distributed under [MIT License](LICENSE.txt)

## <a name="author"></a> Author Information
HippoLab <support@hippolab.ru><br/>London
